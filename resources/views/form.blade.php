<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru</h1>
<br>
<h3>Sign Up Form</h3>
<br>
<table>
    <form action="{{ route('store') }}" method="GET">
        @csrf
        <tr>
            <td><b>First Name</b></td>
        </tr>
        <tr>
            <td><input type="text" name="firstname"></td>
        </tr>
        <tr>
            <td><b>Last Name</b></td>
        </tr>
        <tr><td><input type="text" name="lastname"></td></tr>
        <tr><td><b>Gender</b></td></tr>
        <tr><td><input type="radio" name="gender" id="" value="male">Male</td></tr>
        <tr><td><input type="radio" name="gender" id="" value="male">Female</td></tr>
        <tr><td><b>Nationality</b></td></tr>
        <tr><td><select name="nationality" id="">
            <option value="indonesian">indonesian</option>
            <option value="philiphina">philiphina</option>
            <option value="timor leste">timor leste</option>
        </select></td></tr>
        <tr><td><b>Language Spoken</b></td></tr>
        <tr><td><input type="checkbox" name="language" id="" value="bahasa">Indonesian</td></tr>
        <tr><td><input type="checkbox" name="language" id="" value="english">English</td></tr>
        <tr><td><input type="checkbox" name="language" id="" value="javanese">Javanese</td></tr>
        <tr><td><b>Bio:</b></td></tr>
        <tr><td><textarea name="" id="" cols="30" rows="10"></textarea></td></tr>
        <tr><td>        <input type="submit" value="Submit">
        </td></tr>
    </form>
</table>
</body>
</html>
