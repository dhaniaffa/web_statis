<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function input(Request $request){
        $namaDepan = $request->firstname;
        $namaBelakang = $request->lastname;

        return view('welcom',['depan'=>$namaDepan, 'belakang'=>$namaBelakang]);
    }
}
